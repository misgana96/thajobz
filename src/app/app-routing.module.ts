import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './auth/register/register.component';
import { SigninComponent } from './auth/signin/signin.component';
import { ForgetpasswordComponent } from './auth/forgotpassword/forgotpassword.component';
import { EmploymentComponent } from './auth/employment/employment.component';
import { PrivacyPolicyComponent } from './pages/privacy-policy/privacy-policy.component';
import { HomeComponent } from './pages/home/home.component';
import {ChatComponent} from './pages/chat/chat.component';
import {PaymentComponent} from './pages/payment/payment/payment.component';
import {PriceComponent} from './pages/payment/price/price.component';

import {AuthGuard} from './auth/guard/auth.guard'

import { JobDescriptionComponent } from './pages/job/job-description/job-description.component'
import {JobPageComponent } from './pages/job/job-page/job-page.component';
import { AngularFireAuthGuard, canActivate, redirectUnauthorizedTo } from '@angular/fire/auth-guard';
// const redirectToLoggedIn = () => redirectLoggedInTo(['home']);
const redirectUnauthorizedToLogin = () =>redirectUnauthorizedTo(['signin'])
const routes: Routes = [
  { path:"", component:SigninComponent },
  { path:"home", component:HomeComponent, canActivate: [AuthGuard]},
  {path:"chat", component:ChatComponent},
  { path:"register", component: RegisterComponent},
  { path:"signin", component: SigninComponent},
  { path:"resetforgetpassword", component: ForgetpasswordComponent},
  { path:"payment", component: PaymentComponent},
  { path:"price", component: PriceComponent},
  { path:"employment", component: EmploymentComponent},
  { path:"privacy-policy", component: PrivacyPolicyComponent},
  { path:"job-description",component: JobDescriptionComponent},
  { path:"job-page",component: JobPageComponent},
  { path:"profile", loadChildren: () => import('./pages/user-profile/user-profile.module').then(m => m.UserProfileModule), canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
