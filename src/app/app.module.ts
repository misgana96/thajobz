import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { NgModule } from '@angular/core';
import { OwlModule } from 'ngx-owl-carousel';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './layout/header/header.component';
import { RegisterComponent } from './auth/register/register.component';
import { SigninComponent } from './auth/signin/signin.component';
import {ForgetpasswordComponent} from './auth/forgotpassword/forgotpassword.component'
import { MainComponent } from './auth/main/main.component';
import { EmploymentComponent } from './auth/employment/employment.component';
import { PrivacyPolicyComponent } from './pages/privacy-policy/privacy-policy.component';
import { FooterComponent } from './layout/footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import {ChatComponent} from './pages/chat/chat.component';
import {PaymentComponent} from './pages/payment/payment/payment.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { JobDescriptionComponent } from './pages/job/job-description/job-description.component';
import {JobPageComponent } from './pages/job/job-page/job-page.component';
import {BasicAuthInterceptor} from './auth/basic-auth.interceptor'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; 
import { AngularFireModule }from '@angular/fire';
import { PriceComponent } from './pages/payment/price/price.component';
import { environment } from 'src/environments/environment.prod';
import {NgxPaginationModule} from 'ngx-pagination';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { JobDescriptionPipe } from './pages/job/job-description/job-description.pipe';
import { ToastrModule } from 'ngx-toastr';
import  {  NgxEmojiPickerModule  }  from  'ngx-emoji-picker';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RegisterComponent,
    SigninComponent,
    MainComponent,
    EmploymentComponent,
    PrivacyPolicyComponent,
    FooterComponent,
    HomeComponent,
    ChatComponent,
    PaymentComponent,
    PriceComponent,
    JobDescriptionComponent,
    JobPageComponent,
    ForgetpasswordComponent,
    JobDescriptionPipe
  ],
  imports: [
    NgxSliderModule,
    NgxSpinnerModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    OwlModule,  
    FormsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase),
    NgxPaginationModule,
    CommonModule,
    NgxEmojiPickerModule.forRoot(),
    BrowserAnimationsModule,
    ToastrModule.forRoot()
    // ToastrModule.forRoot({ timeOut: 1000, positionClass: 'toast-bottom-right' })
  ],
  providers: [
    ReactiveFormsModule,
    {provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
