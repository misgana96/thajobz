import { Injectable } from '@angular/core';
import  {HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http'
import {Observable, BehaviorSubject} from 'rxjs'
import {Router} from '@angular/router'
import {map} from 'rxjs/operators'
import {User} from './user'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private loggedIn = new BehaviorSubject<boolean>(false);
	private currentUserSubject: BehaviorSubject<User>;
	public user: Observable<User>
	accessToken: any

  constructor(private http: HttpClient, private router: Router) { 
  	this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));
  	this.user = this.currentUserSubject.asObservable();

     if (this.currentUserValue) {
          this.loggedIn.next(true);
        }
  }

  public get currentUserValue(): User {
  	return this.currentUserSubject.value;
  }

 	httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
 }

    loadToken() {
        const token = this.currentUserValue['id']
        this.accessToken = token
        return this.accessToken
    }


  get isLoggedIn() {
      return this.loggedIn.asObservable();
    }

  signUpWithEmail(value){
      const authData: User = {
        email: value.value.email, 
        password: value.value.password, 
        firstName: value.value.firstName, 
        lastName: value.value.lastName, 
        profession: value.value.profession
      }
      return this.http.post<any>('workers', authData, this.httpOptions)
      .pipe(map(user=> {
          this.loggedIn.next(true)
          user.authData = window.btoa(value.value.email + ':' + value.value.password);
      		localStorage.setItem('user', JSON.stringify(user));
      		this.currentUserSubject.next(user);
          return user
      }))
    }

  loginWithEmail(email: string, password: string) {
  	return this.http.post<any>('workers/login', {email, password}, this.httpOptions)
  	.pipe(map(user=> {
      this.loggedIn.next(true)
  		user.authData = window.btoa(email + ':' + password);
  		localStorage.setItem('user', JSON.stringify(user));
  		this.currentUserSubject.next(user);
  		return user
  	}))
  }

  requestReset(email: string) {
  	return this.http.post<any>('workers/reset-password', email, this.httpOptions)
  }

  logout() {
    this.loggedIn.next(false)
  	localStorage.removeItem('user');
  	this.currentUserSubject.next(null);
  	this.router.navigate(['/login']);
  }
}
