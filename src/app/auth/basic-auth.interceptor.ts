import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpHeaders,
  HttpInterceptor
} from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import {environment} from '../../environments/environment'
import {AuthService} from './auth.service'

@Injectable()
export class BasicAuthInterceptor implements HttpInterceptor {

  constructor(private authservice: AuthService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
  	const reqUrl = environment.apiUrl;

    let token:string
    let currentUser: any = JSON.parse(localStorage.getItem('user'))

    if (currentUser) {
      token = currentUser.id
    }
    else {token=""}

    if (token) {      
      request = request.clone({
        headers: new HttpHeaders({
          Authorization: token
        })
      });
    }
      request = request.clone({
        headers:request.headers.set(
          "Accept",
          "Application/json"
          ),
        url: reqUrl+""+request.url
      });  

    return next.handle(request).pipe(
      tap(
        event=>{
          if (event instanceof HttpResponse) {
            
          }
        },
        error => {
          if (event instanceof HttpResponse) {
            
          }
        }
        )
      );
  }
}
