import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router, ActivatedRoute} from '@angular/router'
import {Observable} from 'rxjs'
import {Employment} from './employment'
import {EmploymentService} from './employment.service'
import {AuthService} from '../auth.service'


declare var $: any;

@Component({
  selector: 'app-employment',
  templateUrl: './employment.component.html',
  styleUrls: ['./employment.component.scss']
})
export class EmploymentComponent implements OnInit {
  fileUrl: string | ArrayBuffer
  selectedFile: File
  userId: string
  id: string
  usersData: any = {}
  profData: any = {}
  public fileName: any ={}
  isEmployment: boolean = true;
  isSubmited: boolean = false


  constructor(private router: Router,private employmentservice: EmploymentService,
              private authservice: AuthService, private activatedroute: ActivatedRoute,
              private http: HttpClient) { }

  @ViewChild("fileInput", {static: false}) fileInput: ElementRef;
  
  ngOnInit(): void {
    
  }
  uploadFile(event) {
    this.id = this.authservice.currentUserValue['id']
    console.log(this.id)
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.onload = (event) => this.fileUrl = event.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedFile = event.target.files[0];

    }
    else {
      this.fileUrl = null;
    }

    const file: HTMLInputElement = this.fileInput.nativeElement
    this.employmentservice.uploadResume(file.files).subscribe(res=> {
      this.fileName = res['result'].files[this.selectedFile['name']][0].name
      console.log(this.fileName)
      this.fileUrl = `https://jobz6.herokuapp.com/api/files/docs/download/${this.fileName}`
      console.log(this.fileUrl)
      this.http.patch(`workers/${this.id}`, {resumeUrl: this.fileUrl})
      .subscribe(res=> {
        console.log('sucess', res)
      })
    })

  }


  insertUserProfession(Employment) {
        this.userId = this.usersData['id']
        const education: Employment = {qualification: Employment.qualification, course: Employment.course, courseType: Employment.courseType, school: Employment.school, specialization: Employment.specialization, workerId: this.userId}
        return this.http.post('education', education);
  }
  onApply(form: NgForm) {
    this.isSubmited = true;
    this.insertUserProfession(form).subscribe(res=>{
      console.log(res)
    })
    // this.employmentservice.insertUserJobDetail(this.profData).then((res)=>{
    //   alert('email verification has been sent, check your email')
    //   this.router.navigate(['/signin'])
    // }).catch((error)=>{
    //   console.log(error)
    // })
  }
  ngAfterViewInit(): void {
    $(document).ready(function () {
      $('.filebtn').on('click', function () {
        $('.file').trigger('click');
      });

      $('.file').on('change', function () {
        var fileName = $(this)[0].files[0].name;
        $('#file-name').val(fileName);
      });
    })
  }

  goSecondStep() {
    this.isEmployment = !this.isEmployment
  }

}
