import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {Router} from '@angular/router'
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class EmploymentService {
	constructor(private http: HttpClient) { }

       uploadResume(file: FileList) {
            const formData = new FormData()
            for (let i = 0; i < file.length; i++) {
                formData.append(file[i].name, file[i])
            }
            return this.http.post<any>(`files/docs/upload`, formData)
       }
}