export interface Employment {
	qualification: string,
	course: string,
	courseType: string,
	school: string,
	specialization: string,
	workerId: string
}
