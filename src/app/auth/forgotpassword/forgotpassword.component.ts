import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Router} from '@angular/router'

import {AuthService} from '../auth.service'


@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.scss']
})
export class ForgetpasswordComponent implements OnInit {
	passwordResetEmail: string
	successMessage: string;
	constructor(private router: Router, private authservice: AuthService) { }

	ngOnInit(): void {
	}

	onApply(form: NgForm) {		
		if (!this.passwordResetEmail) {
			alert('Type in your email first')
		}
		this.authservice.requestReset(this.passwordResetEmail).subscribe(data=> {
			form.reset();
			this.successMessage = "Reset password link send to email sucessfully.";
			setTimeout(()=> {
				this.successMessage = null;
				this.router.navigate(['/signin'])
			}, 3000);
		})
	}
}