import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth'
import {AngularFireDatabase} from '@angular/fire/database'
import {Router} from '@angular/router'
import firebase from 'firebase/app'

@Injectable({
  providedIn: 'root'
})
export class ForgetpasswordService {

  constructor(private auth: AngularFireAuth, private db: AngularFireDatabase,
  				private router: Router) { }


  ForgotPassword(passwordResetEmail) {
  	return this.auth.sendPasswordResetEmail(passwordResetEmail)
  }
}
