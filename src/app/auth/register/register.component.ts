import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import {User} from '../user'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  resData: any = {}
  id: string
  constructor(
    private router:Router,
    private authService:AuthService
  ) { }

  ngOnInit(): void {
  }

  onSubmit(form:NgForm){
    this.authService.signUpWithEmail(form).subscribe(res=>{
      this.resData = res
      // this.id = res['id']
      console.log(this.resData);
      this.router.navigate(['/employment'],{queryParams: {id:this.id}})
    })
     form.reset()
  }
}
