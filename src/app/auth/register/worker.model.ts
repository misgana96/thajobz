export interface Worker {
	email: string,
	password: string,
	firstName: string,
	lastName: string,
	profession: string
}
