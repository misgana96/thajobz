import { Component, Injectable, OnInit, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router'


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  @ViewChild('inputPassword') inputPasswordRef: ElementRef;
  @ViewChild('passwordIcon') passwordIconRef: ElementRef;

  @Output()
  myEmitter: EventEmitter<number> = new EventEmitter();
  constructor(private authservice:AuthService, private router:Router) {

   }

  ngOnInit(): void {

  }

  onSubmit(form:NgForm){
    if(!form.valid){
      return;
    }
    const email=form.value.email;
    const password=form.value.password;
    this.authservice.loginWithEmail(email,password)
    .subscribe(data=> {
      this.router.navigate(['/home'])
    })
  }
  showHidePassword(): void {
    if(this.inputPasswordRef.nativeElement.type == "text"){
      this.inputPasswordRef.nativeElement.type = 'password';
      this.passwordIconRef.nativeElement.classList.add( "fa-eye-slash" );
      this.passwordIconRef.nativeElement.classList.remove( "fa-eye" );
    }else if(this.inputPasswordRef.nativeElement.type == "password"){
        this.inputPasswordRef.nativeElement.type = 'text';
        this.passwordIconRef.nativeElement.classList.remove( "fa-eye-slash" );
        this.passwordIconRef.nativeElement.classList.add( "fa-eye" );
    }
  }
}
