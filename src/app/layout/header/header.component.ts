import { Component, OnChanges, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {AuthService} from '../../auth/auth.service'
import { SearchService } from './search.service'
import {Observable} from 'rxjs'
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit{

  isLoggedIn$: Observable<boolean>
  title:string
  location:string
  constructor(public authservice:AuthService, private router: Router, private search:SearchService) { }

  ngOnInit(): void {
    this.isLoggedIn$ = this.authservice.isLoggedIn
    const user = this.authservice.currentUserValue
    console.log(user['userId'])
  }
  logOut(){
    this.authservice.logout()
    this.router.navigate(['/signin'])
  }
  searchByTitle(event){
  console.log('event',event)
  this.search.setTitle(event)
  }
  searchByLocation(event){
    console.log('eventloc',event)
    this.search.setLocation(event)
  }

  addItem(title:string,location:string){
  console.log('daa',title,location)
  this.searchByTitle(title)
  this.searchByLocation(location)
  }

  goToJobPage() {
    this.router.navigate(['/job-page'])
  }
}
