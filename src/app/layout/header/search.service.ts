import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SearchService {
  private title = new BehaviorSubject<string>(undefined);
  private location=new BehaviorSubject<string>(undefined);
  private _title: Observable<string> = this.title.asObservable();
  private _location: Observable<string> = this.location.asObservable();
  constructor(private router: Router) { }
  
  setTitle(title:any){
    this.title.next(title)
    this.router.navigate(['/job-page'])
  
  }
  setLocation(location:any){
   this.location.next(location)
  }
  get getTitle(){
   return this._title
  }
  get getLocation(){
    return this._location
   }
   
}
