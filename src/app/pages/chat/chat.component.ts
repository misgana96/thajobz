import { Component, OnInit, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ChatService } from './chat.service'
import { NgForm } from '@angular/forms';
import * as moment from 'moment';
import { finalize } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})

export class ChatComponent implements OnInit {
  jobId: string;
  userId: string;
  peerConnection: any
  apiUrl = 'https://jobz6.herokuapp.com/api/files'
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      Authorization: JSON.parse(localStorage.getItem('user')).id
    })
  };
  peerConnection2: any
  contact: any
  chatId: any
  message: string = '';
  fileName: string
  typeName: string
  company: any
  toggled: boolean = false;
  uploadProgress: Observable<number>;
  downloadURL: Observable<string>;
  showEmojiPicker = false;
  @ViewChild("localVideo") localVideo: ElementRef
  @ViewChild("remoteVideo") remoteVideo: ElementRef
  @ViewChild('scroller') scroller: ElementRef
  scrolltop: number = null
  // @ViewChild("remoteVideo") remoteVideo: any;
  // localVideo = document.getElementById("localVideo");
  //  remoteVideo = document.getElementById("remoteVideo");
  localStream: any
  remoteStream: any
  configuration = {
    iceServers: [
      {
        urls: [
          'stun:stun1.l.google.com:19302',
          'stun:stun2.l.google.com:19302',
        ],
      },
    ],
    iceCandidatePoolSize: 10,
  };
  constraints = {
    'video': true,
    'audio': true
  }
  messages: any[] = []
  ChatRooms: any[] = []

  ngOnInit(): void {
    this.activatedRouter.queryParams.subscribe(params => {
      this.jobId = params.jobId
    })
    this.checkExixtance()
    // this.getMessage()
    this.getJobsList()
    this.contactInfo()
    // this.startChat()

    //  this.localVideo.nativeElement.srcObject=this.video.localVideo
  }
  startChat() {
    this.chat.startChat(this.jobId).subscribe(res => {
      if (res) {
        this.chatId = (res as any).id
        this.getMessage()
      }

    })
  }
  scrollToBottom(): void {
    this.scroller.nativeElement.scrollTop = this.scroller.nativeElement.scrollHeight
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  public iscollapsed = false;
  constructor(private http: HttpClient, private router: Router, private activatedRouter: ActivatedRoute, private chat: ChatService) {
  }
  setMessage(form: NgForm) {
    if (!form.valid) {
      return;
    }
    this.message = form.value.message;
    var type = 'text'
    this.chat.createMessage(this.message, this.chatId).subscribe(res => {
      if (res) {
        this.getMessage()
      }

    })
    form.reset()
  }
  toggleEmojiPicker() {
    this.showEmojiPicker = !this.showEmojiPicker;
  }
  getMessage() {
    this.chat.getMessage(this.chatId).subscribe(resData => {
      this.messages = (resData as any)
    })

  }
  getJobsList() {
    this.chat.getJob(this.jobId).subscribe(res => {
      this.ChatRooms = (res as any)
    })
  }
  checkExixtance() {
    this.chat.checkChatId(this.jobId).subscribe(res => {
      if ((res as any).length > 0) {
        this.chatId = (res as any)[0].id
        this.getMessage()
      }
      else {
        this.startChat()
      }
    })
  }
  handleSelection(event) {
    this.message += event.char;
  }
  reload(info: any) {
    this.router.navigate(['/chat'], { queryParamsHandling: 'merge', relativeTo: this.activatedRouter, queryParams: { jobId: info.jobId } })
    this.jobId = info.jobId
    this.contactInfo()
    this.checkExixtance()
  }
  contactInfo() {
    this.chat.getContactInfo(this.jobId).subscribe(res => {
      if ((res as any).length > 0) {
        const obj = Object.assign({}, res);
        this.contact = obj[0]
      }
    })
  }
  close() {
    this.router.navigate(['/home'])
  }
  uploadFile(event: any, type: string) {
    this.typeName = type
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      const data = new FormData()
      data.append('file', file, file.name)
      this.chat.uploadFile(data, this.typeName).subscribe(res => {
        if (res) {
          this.fileName = res['result'].files.file[0].name
          this.chat.downloadFile(this.fileName, this.chatId, file.name, this.typeName).subscribe(res => {
            if (res) {
              this.getMessage()
            }
          })
        }
      })
    }
  }


}


