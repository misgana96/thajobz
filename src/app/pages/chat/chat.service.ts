import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from '../../auth/auth.service'

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  httpOptions = {
    headers: new HttpHeaders({
      Authorization: JSON.parse(localStorage.getItem('user')).id
    })
  };
  constructor(private http: HttpClient, private authservice: AuthService) {

  }
  createMessage(message: string, chatId: string) {
    const data = {
      content: message,
      workerId: JSON.parse(localStorage.getItem('user')).userId,
      chatId: chatId,
    }
    return this.http.post('messages', data)
  }
  getMessage(chatId: string) {
    const data = {
      where: {
        chatId: chatId
      }
    }
    const filter = JSON.stringify(data)
    return this.http.get(`messages?filter=${filter}`)
  }
  getJob(jobId: string) {
    const data = {
      where: {
        workerId: JSON.parse(localStorage.getItem('user')).userId,

      },
      include: [{
        relation: 'job', scope: {
          include: {
            relation: 'company'
          }
        }
      }, { relation: 'worker' }]
    }
    const filter = JSON.stringify(data)
    return this.http.get(`applications?filter=${filter}`)
  }
  startChat(value) {
    const user = JSON.parse(localStorage.getItem('user'))
    const data = {
      jobId: value,
      workerId: user.userId
    }
    return this.http.post('chats', data)
  }
  checkChatId(value) {
    const user = JSON.parse(localStorage.getItem('user'))
    const data = {
      // include: [ "company"],
      where: {
        jobId: value,
        workerId: user.userId
      }
    }

    const filter = JSON.stringify(data)
    return this.http.get(`chats?filter=${filter}`)
  }
  getChatId() {
    return this.http.get('chats')
  }
  getContactInfo(value: any) {
    const data = {
      include: ["company"],
      where: {
        id: value,
      }
    }

    const filter = JSON.stringify(data)
    return this.http.get(`jobs?filter=${filter}`, this.httpOptions)
  }
  uploadFile(data: any, typeName: string) {
    return this.http.post(`https://jobz6.herokuapp.com/api/files/${typeName}/upload`, data, this.httpOptions)
  }
  downloadFile(name: any, chatId: string, type: string, typeName) {
    const data = {
      content: type,
      workerId: JSON.parse(localStorage.getItem('user')).userId,
      chatId: chatId,
      filesUrl: [
        typeName,
        `https://jobz6.herokuapp.com/api/files/${typeName}/download/${name}`
      ]
    }
    return this.http.post('messages', data, this.httpOptions)
  }
}
