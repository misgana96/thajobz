import { Component, OnChanges, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { HomeService } from './home.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
  // @ViewChild(ToastContainerDirective, { static: true })
  // toastContainer: ToastContainerDirective;
  userId: string
  Vacancies: any[] = [];
  ArivalImages: any[] = [];
  HomeImages = ['assets/images/banner.jpg', 'assets/images/banner.jpg', 'assets/images/banner.jpg'];
  PartnerImages = ['assets/images/google.png', 'assets/images/intel.png', 'assets/images/microsoft.png', 'assets/images/HP-Logo.png'];
  totalLength: any
  count: number = 6
  test: ""
  jobId: string
  isLoading = false
  title = undefined
  location = undefined
  min: number = 6
  viewMore: boolean = false
  viewLess: boolean = false
  SlideOptions = {
    loop: true,
    margin: 10,
    nav: true,
    dots: true,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 1
      }
    }
  };

  ArivalSlideOptions = {
    loop: true,
    margin: 15,
    items: 3, // THIS IS IMPORTANT
    nav: true,
    dots: false,
    responsive: {
      480: { items: 1 }, // from zero to 480 screen width 4 items
      768: { items: 2 }, // from 480 screen widthto 768 6 items
      1024: { items: 3 }  // from 768 screen width to 1024 8 items
    }
  };

  PartnersSlideOptions = {
    loop: true,
    margin: 15,
    center: true,
    nav: true,
    dots: false,
    responsive: {
      320: {
        items: 2
      },
      767: {
        items: 4
      },
      1000: {
        items: 5
      }
    }
  };

  constructor( private router: Router, private jobs: HomeService, private toastr: ToastrService) {

  }

  ngOnInit() {
    this.getCount()
    this.getJobsList();
  }
  goToSignIn() {
    this.router.navigate(['/signin']);
  }
  goToApply() {
    this.router.navigate(['/signin'])
  }
  checkExistance(value) {
    this.jobId = value
    this.jobs.checkApplyJob(value).subscribe(res => {
      if ((res as any).length > 0) {
        this.toastr.error('You Already Applied');
      }
      else {
        this.applyForJob(this.jobId)
      }
    })
  }
  getCount() {
    this.jobs.getCount().subscribe(res => {
      this.totalLength = (res as any)
      this.showMore()
    })
  }

  showMore() {
    if (this.count > this.totalLength.count && this.totalLength.count > this.min) {
      this.viewMore = false
      this.viewLess = true
      this.jobs.getJob(this.totalLength.count).subscribe(res => {
        this.Vacancies = (res as any)
      })
    }
    else if (this.totalLength.count < this.min) {
      this.viewMore = false
      this.viewLess = false
      this.jobs.getJob(this.totalLength.count).subscribe(res => {
        this.Vacancies = (res as any)

      })
    }
    else {
      this.viewMore = true
      this.viewLess = false
      this.count += 6
      this.jobs.getJob(this.count).subscribe(res => {
        this.Vacancies = (res as any)

      })
    }
  }
  showLess() {

    if (this.count <= this.min) {
      this.viewMore = true
      this.viewLess = false
      this.jobs.getJob(this.min).subscribe(res => {
        this.Vacancies = (res as any)

      })
    }
    else {
      this.count -= 6
      this.jobs.getJob(this.count).subscribe(res => {
        this.Vacancies = (res as any)

      })
    }
  }

  goToJobPage() {
    this.router.navigate(['/job-page'])
  }
  getJobsList() {
    this.jobs.getJobs().subscribe(res => {
      this.ArivalImages = (res as any)

    })
  }


  applyForJob(values) {
    this.jobs.apply(values).subscribe(res => {
      if (res) {
        this.toastr.success('Successfuly register!');
      }

    })
  }
}
