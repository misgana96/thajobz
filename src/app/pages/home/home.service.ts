import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class HomeService {
  jobs: Observable<any[]>;
  userId: string;
  childKey: string;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: JSON.parse(localStorage.getItem('user')).id
    })
  };
  constructor(private http: HttpClient,
    public route: ActivatedRoute) { }
  getJobs() {
    const data = {
      order: 'createdAt DESC',
      limit: 10,
      include: {
        relation: 'company'
      }
    }
    const filter = JSON.stringify(data);
    return this.http.get(`jobs?filter=${filter}`)
  }
  getJob(limit: number) {
    const data = {
      order: 'createdAt DESC',
      limit: limit,
      include: {
        relation: 'company'
      }
    }
    const filter = JSON.stringify(data)
    return this.http.get(`jobs?filter=${filter}`)
  }
  getJobsByFilter(searchByTitle: string, searchByLocation: string) {
    const data = {
      where: {
        title: searchByTitle || undefined,
        location: searchByLocation || undefined
      },
      include: {
        relation: 'company'
      }
    }
    const filter = JSON.stringify(data)
    return this.http.get(`jobs?filter=${filter}`)
  }
  getCount() {
    return this.http.get('jobs/count', { headers: JSON.parse(localStorage.getItem('user')).id })
  }
  checkApplyJob(value) {
    const user = JSON.parse(localStorage.getItem('user'))
    const data = {
      // include: [ "company"],
      where: {
        jobId: value,
        workerId: user.userId
      }
    }

    const filter = JSON.stringify(data)
    return this.http.get(`applications?filter=${filter}`, this.httpOptions)
  }
  apply(value) {
    const user = JSON.parse(localStorage.getItem('user'))
    const data = {
      jobId: value,
      workerId: user.userId
    }
    return this.http.post('applications', data, this.httpOptions)
  }

}
