export interface Home {
   title:string,
   role:string,
   description:string,
   experience:string,
   salary: 0,
   location:string,
   contractType:string,
   education:string,
   id:string,
   companyId:string,
}
