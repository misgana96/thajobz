import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';
import "firebase/analytics";                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
import { JsonpClientBackend } from '@angular/common/http';
import { JobDescriptionService } from './job-description.service';

@Component({
  selector: 'app-job-description',
  templateUrl: './job-description.component.html',
  styleUrls: ['./job-description.component.scss']
})

export class JobDescriptionComponent implements OnInit {
  job:any
  id:string;
  company:any
  constructor(private router:Router,private activatedRouter:ActivatedRoute,private des:JobDescriptionService) { }

  ngOnInit(): void {

    this.activatedRouter.queryParams.subscribe(params=>{
     this.job=params
     this.id=params.id
     this.getCompany()
    })

  
  }
  goToChat(){
    this.router.navigate(['/chat'],{queryParams:{jobId:this.id}})
  }
  applyForJob(values){
    this.des.apply(values).subscribe(res=>{
  
    })
  }
  getCompany(){
    this.des.getCompany(this.job.companyId).subscribe(res=>{
 this.company=res
    })
  }
}
