import { Injectable } from '@angular/core';
import firebase from "firebase/app";
import {ActivatedRoute} from '@angular/router'
import { Observable } from 'rxjs';																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																													
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})

export class JobDescriptionService {
httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    Authorization: JSON.parse(localStorage.getItem('user')).id
  })
};
  constructor(private http: HttpClient,
    public route: ActivatedRoute) { }

    apply(value){
      const user= JSON.parse(localStorage.getItem('user'))
      const data={
        jobId:value,
        workerId:user.userId
      }
      return this.http.post('applications',data,this.httpOptions)
    }
    getCompany(id){
      return this.http.get(`companies/${id}`)
    }
}