import { Component, OnInit } from '@angular/core';
import { Options } from '@angular-slider/ngx-slider';
import { JobPageService } from './job-page.service';
import * as moment from 'moment';
import * as _ from 'lodash';
import { NgxSpinnerService } from "ngx-spinner";
import { SearchService } from 'src/app/layout/header/search.service';
@Component({
  selector: 'app-job-page',
  templateUrl: './job-page.component.html',
  styleUrls: ['./job-page.component.scss']
})
export class JobPageComponent implements OnInit {
  isLoading:boolean=false;
  newDate=moment().subtract(15, 'days').format();
  workHome:string;
  toggle = true;
  partTime:string;
  fullTime:string;
  it:string;
  contraction:string;
  education:string;
  sales:string;
  value: number = 0;
  totalLength:number=0;
  highValue: number = 4000;
  options: Options = {
    floor: 0,
    ceil: 10000
  };
  jobType:any[]= [];
  industry:any[]=[];
  // Industries:any[]= [];
  vacancies: any[] = [];
  newVacancies:any[]
  filter={
    // include: [ "company"],
    where:{
      title:undefined,
      location:undefined,
      createdAt:undefined,
      salary:undefined,
      contractType:undefined
    },
    include: {
      relation:'company',
      scope: {
        where:{
          industry:undefined
        }
      }
    }
  }
  p: number = 1;
  constructor(private jobs:JobPageService,private searchService :SearchService,private spinner: NgxSpinnerService) { 
  
  }

  ngOnInit(): void {
    this.searchDataByTitle()
    this.searchDataByLocation()
    this.getJobsList();
    this.searchData()

  }
 openNav() {
    document.getElementById("mySidenav").style.width = "250px";
  }
  searchData(){
    this.searchService.getTitle.subscribe(res=>{
 
    })
  }
  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }
  getJobsList(){
    this.spinner.show();
  if(this.newDate!=undefined){
    this.filter.where.createdAt={gte: new Date(this.newDate)}
  }
    this.jobs.getJobs(this.filter).subscribe(res=>{
      this.vacancies=(res as any)
    })
  }
  filterBy(value){
   switch (value) {
     case 'date1': 
       this.newDate=moment().subtract(15, 'days').format();                         
       break;
     case 'date2':
       this.newDate=moment().subtract(1, 'month').format();                         
       break;
     case 'date3':
       this.newDate=moment().subtract(6, 'month').format();                         
       break;
     default:
       this.newDate=moment().subtract(15, 'days').format();                         
       break;
  }
  this.getJobsList()
 }

 filterByJobType(property:string,rule:any){
  //  this.filters[property]=val=>val==rule
  this.jobType.push(rule)
  this.filter.where.contractType= this.jobType[this.jobType.length - 1]
  this.getJobsList()

 }
 filterByIndustry(property:string,rule:any){

this.industry.push(rule)
this.filter.include.scope.where.industry=this.industry[this.industry.length-1]
this.getJobsList()
 }
 filterRange(property:string,rule1:any,rule2:any){
  //  this.filters[property]=val=>rule1<val&&val<rule2
  this.filter.where.salary={between:[rule1,rule2]}
  this.getJobsList()
 }
 searchDataByTitle(){
  this.searchService.getTitle.subscribe(res=>{
   if(res!=undefined||res!=null){
    if(res===""){
      this.filter.where.title=undefined
      this.getJobsList()
    }else{
      this.filter.where.title=res
      this.getJobsList()
    }
    
   }
   else{
  
    this.filter.where.title=undefined
     this.getJobsList()
   }
  })
}
searchDataByLocation(){
  this.searchService.getLocation.subscribe(res=>{
   if(res!=undefined || res!=null){
    if(res===""){
      this.filter.where.location=undefined
      this.getJobsList()
    }else{
      this.filter.where.location=res
      this.getJobsList()
    }

   }
   else{
    this.filter.where.location=undefined
     this.getJobsList()
   }
  })
}
 filterRemove(property:string){
  this.jobType= this.jobType.filter(item => item !== property)
  this.filter.where.contractType= this.jobType[this.jobType.length - 1]
  this.getJobsList()

 }
 buttonToggle(property:string,rule:string){
  if(this.toggle){
    this.filterByJobType(property,rule);
    this.toggle = !this.toggle;
  }
  else{
    this.filterRemove(property);
    this.toggle = !this.toggle;
  }
 } 
}
