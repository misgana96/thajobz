import { TestBed } from '@angular/core/testing';

import { JobPageService } from './job-page.service';

describe('JobPageService', () => {
  let service: JobPageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JobPageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
