import { query } from '@angular/animations';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class JobPageService {
  constructor(private http:HttpClient) { }
  getJobs(value){
  const filter=JSON.stringify(value);
  return this.http.get(`jobs?filter=${filter}`)
  }
}
