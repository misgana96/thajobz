import {Component, OnInit} from '@angular/core'
import { componentFactoryName } from '@angular/compiler'
import {Router, ActivatedRoute} from '@angular/router'
import { Observable } from 'rxjs';

@Component({
    selector: 'app-payment',
    templateUrl: './payment.component.html',
    styleUrls: ['./payment.component.scss']
})

export class PaymentComponent implements OnInit {

	paymentPlans: any = {};
    constructor(private router:Router,private activatedRouter:ActivatedRoute) { }

    ngOnInit(): void {
   		this.activatedRouter.queryParams.subscribe(params=>{
     	this.paymentPlans=params
     	console.log(this.paymentPlans)
     	this.paymentPlans=params.key

    });
    }  

}
