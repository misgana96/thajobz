import {Component, OnInit} from '@angular/core'
import { componentFactoryName } from '@angular/compiler'
import {Router} from '@angular/router'


@Component({
    selector: 'app-price',
    templateUrl: './price.component.html',
    styleUrls: ['./price.component.scss']
})

export class PriceComponent implements OnInit {
	constructor() { }
    ngOnInit(): void {
    	
    };	
	paymentPlans = {
		basicPlan: {
			name: 'Basic',
			offered: 'Get 2 month subscription in this plan',
			price: '$15/month',
			packages:{
				time: '2 Month',
				job: 'applay for 25 jobs',
				chat: 'chat for 10 jobs',
				resume: 'your resume submit 10 company',
				notify: '20 times specially notify',
				discount: 'Up to 30% Discount *only Today',
				card: 'Use credit card 100% flat discount'
			}
		},
		silverPlan: {
			name: 'Silver',
			offered: 'Get 5 month subscription in this plan',
			price: '$25/month',
			packages:{
				time: '5 Month',
				job: 'applay for 50 jobs',
				chat: 'chat for 25 jobs',
				resume: 'your resume submit 30 company',
				notify: '50 times specially notify',
				discount: 'Up to 50% Discount *only Today',
				card: 'Use credit card 5% flat discount'
			}
		},
		goldPlan: {
			name: 'Gold',
			offered: 'Get 10 month subscription in this plan',
			price: '$15/month',
			packages:{
				time: '10 Month',
				job: 'applay for 100 jobs',
				chat: 'chat for 75 jobs',
				resume: 'your resume submit 200 company',
				notify: '70 times specially notify',
				discount: 'Up to 50% Discount *only Today',
				card: 'Use credit card 5% flat discount'
			}
		}
	}
}