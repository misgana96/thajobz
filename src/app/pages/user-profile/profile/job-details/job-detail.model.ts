export interface JobDetailData {
    description: string,
    company: string,
    salary: string,
    location: string,
    start: Date,
    end: Date,
    industry: string,
    workerId: string,
}

export interface JobsAlertData {
    role: string,
    salaryMin: string,
    salaryMax: string,
    area: string,
    remote: Date,
    workerId: string,
}