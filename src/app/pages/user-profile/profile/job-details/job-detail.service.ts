import { Injectable, Inject } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http'
import {JobDetailData} from './job-detail.model'
import {JobsAlertData} from './job-detail.model'
import { first } from 'rxjs/operators';
import {AuthService} from '../../../../auth/auth.service'


@Injectable({
    providedIn: 'root'
})

export class JobDetalService {
    userId: any    
    constructor (private http: HttpClient, private authservice: AuthService){}

    getJobDetail() {
        const data={
            where:{
              workerId:this.authservice.currentUserValue['userId']
            }
        }
        const filter=JSON.stringify(data)        
        return this.http.get<any>(`employments?filter=${filter}`)
    }

    insertJobDetail(value, id) {
        const user = this.authservice.currentUserValue
        this.userId = this.authservice.currentUserValue['userId']
        const data: JobDetailData= {
            description: value.description,
            company: value.general,
            salary: value.newJob,
            location: value.jobNotification,
            start: value.jobSound,
            end: value.newChat,
            industry: value.chatNotification,
            workerId: this.userId,
        }                
        if (id != null) {           
            return this.http.patch<any>(`employments/${id}`, value) 

        }

        else{
            return this.http.post<any>(`employments`, value)
        }
    }
}