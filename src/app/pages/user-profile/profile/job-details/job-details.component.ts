import { Component, OnInit, Inject } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { JobDetalService } from './job-detail.service';



@Component({
  selector: 'app-job-details',
  templateUrl: './job-details.component.html',
  styleUrls: ['./job-details.component.scss']
})

export class JobDetailsComponent implements OnInit {

  isSubmitted: boolean;
  userId: string
  jobsData: any = {}
  id: any

  constructor(
    private router: Router, private jobdetailservice: JobDetalService) { }

  ngOnInit(): void {
    this.jobdetailservice.getJobDetail().subscribe(res=> {
      this.jobsData = Object.assign({}, ...res)
      this.id = this.jobsData.id
    })
  }

  onApply(form: NgForm) {
    this.jobdetailservice.insertJobDetail(this.jobsData, this.id)
    .subscribe(
      res => {
        this.router.navigate(['/profile/notification'], {queryParams:{id:res}})
      }
    )
  }

}
