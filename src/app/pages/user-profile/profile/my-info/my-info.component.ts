import { Component, OnInit, Inject, ElementRef, ViewChild } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http'
import {FileToUpload} from './user-information.model'
import { NgForm, FormBuilder, FormGroup} from '@angular/forms';
import { map } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs/operators';
import {Observable, BehaviorSubject} from 'rxjs'
import { UserInfoService } from './user-info.service';
import {AuthService} from '../../../../auth/auth.service'
import {User} from '../../../../auth/user'


@Component({
  selector: 'app-my-info',
  templateUrl: './my-info.component.html',
  styleUrls: ['./my-info.component.scss']
})
export class MyInfoComponent implements OnInit {
  

  file: File
  message: string[] = []
  httpOptions: any
  imageUrl: any
  pictureUrl: any = {}
  img: HTMLImageElement
  selectedImage: File;
  isSubmitted: boolean;
  usersData: any= {}
  userId: string
  public fileName: any ={}

  container: any = "test"

  @ViewChild("fileUpload", {static: false}) fileUpload: ElementRef;
  files = [];
  constructor(
    private router:Router, private route: ActivatedRoute, 
    private authservice: AuthService, private http: HttpClient,
    private userinfoservice: UserInfoService,private formBuilder: FormBuilder
    ){}

  ngOnInit(): void {

    this.userinfoservice.getUserInformation().subscribe(res=>{
      this.imageUrl = res[0].pictureUrl
      this.userinfoservice.image.next(this.imageUrl)
    })

    this.userinfoservice.getCurrentUser().subscribe(res=>{
      this.usersData = res
    })

    this.userinfoservice.getUserFile().subscribe(res=> {
    })
}
  
  onFileChange(event) {
    this.userId = this.authservice.currentUserValue['userId']
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.onload = (event) => this.imageUrl = event.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedImage = event.target.files[0]
    }
    else {
      this.imageUrl = null;
    }

    const fileInput: HTMLInputElement = this.fileUpload.nativeElement
    this.userinfoservice.uploadImage(fileInput.files).subscribe(res=> {

      this.fileName = res['result'].files[this.selectedImage['name']][0].name
      this.imageUrl = `https://jobz6.herokuapp.com/api/files/images/download/${this.fileName}`
      this.http.patch(`workers/${this.userId}`, {pictureUrl: this.imageUrl})
      .subscribe(res=> {
        })

    })

  }

  onApply(form:NgForm) {
    this.isSubmitted =true;
    this.userinfoservice.insertUserInformationDetail(form)
    .subscribe(
      res=> {  
        this.router.navigate(['/profile/profession'] ,{queryParams:{id:res}})
      })
    
    }
  
}