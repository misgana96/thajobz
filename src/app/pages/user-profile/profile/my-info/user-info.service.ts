import { Injectable, Inject } from '@angular/core';
import {userInformationData} from './user-information.model'
import {FileToUpload} from './user-information.model'
import {HttpClient, HttpHeaders} from '@angular/common/http'
import {observable, BehaviorSubject} from 'rxjs'
import { first } from 'rxjs/operators';
import {User} from '../../../../auth/user'
import {AuthService} from '../../../../auth/auth.service'

@Injectable({
    providedIn: 'root'
  })

export class UserInfoService {

    accessToken: any
    userId: string
    httpOptions: any
    public image: any

    

    constructor (private http: HttpClient, private authservice: AuthService){
            this.image = new BehaviorSubject([]);
    }

    get getImage() {
        return this.image.asObservable()
    }
    getUserInformation() {
    const data={
    where:{
      id:this.authservice.currentUserValue['userId']
        }
      }
        const filter=JSON.stringify(data)
        return this.http.get<any>(`workers?filter=${filter}`);
    }
        
    getCurrentUser() {
        this.userId = this.authservice.currentUserValue['userId']
        return this.http.get<any>(`workers/${this.userId}`);
    }

    getUserFile() {
        return this.http.get('files'); 
    }

    uploadImage(file: FileList) {
        const formData = new FormData()
        for (let i = 0; i < file.length; i++) {
            formData.append(file[i].name, file[i])
        }
        return this.http.post<any>(`files/images/upload`, formData)
    }
    insertUserInformationDetail(value) {
        const user = this.authservice.currentUserValue

        
        const data: userInformationData = {
            firstname: value.firstname,
            lastname: value.lastname,
            email: value.email,
            date_of_birth: value.date_of_birth,
            street: value.street,
            tower: value.tower,
            city: value.city,
            password: value.password,
            state: value.state,
            country: value.country,
            zip: value.zip,
            notification_status: value.notification_status,
        }
        if (user) {
            return this.http.patch<any>(`workers/${this.userId}`, data);
        }
    }

} 