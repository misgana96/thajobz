export interface userInformationData{
    firstname: string,
    lastname: string,
    email: string,
    date_of_birth: string,
    street: string,
    tower: string,
    city: string,
    state: string,
    country: string,
    password: string,
    zip: string,
    notification_status: boolean,
    
}

export class FileToUpload {

    fileName: string = "";
    fileSize: number = 0;
    fileType: string = "";
    lastModifiedTime: number = 0;
    lastModifiedDate: Date = null;
    fileAsBase64: string = "";
}