import { Component, OnInit, Inject } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { from } from 'rxjs';
import {NotificationService} from './notification.service'
import {AuthService} from '../../../../auth/auth.service'

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  isSubmitted: boolean
  userId: any
  id: any
  notifyData: any= {}
  userConfigNotification: any
  constructor(
    private router: Router, private notificationservice: NotificationService,
    private authservice: AuthService) { }

  ngOnInit(): void {
    this.notificationservice.getNotificationDetail().subscribe(res=>{
      this.userConfigNotification = Object.assign({}, ...res)
      this.id = this.userConfigNotification.id
    })
  }

  onApply(form: NgForm) {
    this.notificationservice.insertNotificationDetail(form, this.id)
    .subscribe(
      res =>{
        this.router.navigate(['/profile/settings'],  {queryParams:{id:res}})
      }
    )
  }

}
