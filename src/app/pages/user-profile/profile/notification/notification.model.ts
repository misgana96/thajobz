export interface NotificationData {
    general: boolean,
    newJob: boolean,
    jobNotification: boolean,	
    jobSound: boolean,
    newChat: boolean,
    chatNotification: boolean,
    chatSound: boolean,
    workerId: string
}	