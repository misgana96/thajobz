import { Injectable, Inject} from '@angular/core'
import {HttpClient, HttpHeaders} from '@angular/common/http'
import {NotificationData} from './notification.model'
import { map } from 'rxjs/operators';
import {AuthService} from '../../../../auth/auth.service'




@Injectable({
    providedIn: 'root'
})

export class NotificationService {
    userId: any
    id: any
    workerId: any
    constructor (private http: HttpClient, private authservice: AuthService){}

    getNotificationDetail() {
        const data={
            where:{
              workerId:this.authservice.currentUserValue['userId']
            }
        }
        const filter=JSON.stringify(data)
        return this.http.get<any>(`alertConfigs?filter=${filter}`)
    }

    insertNotificationDetail(value, id) {
        const user = this.authservice.currentUserValue    
        this.userId = this.authservice.currentUserValue['userId']
        const data: NotificationData= {
            general: value.value.general,
            newJob: value.value.newJob,
            jobNotification: value.value.jobNotification,
            jobSound: value.value.jobSound,
            newChat: value.value.newChat,
            chatNotification: value.value.chatNotification,
            chatSound: value.value.chatSound,
            workerId: this.userId,
        }

        if (id != null) {
            return this.http.patch<any>(`alertConfigs/${id}`, data)
        }

        else {
            return this.http.post<any>('alertConfigs', data)
        }
    }
}