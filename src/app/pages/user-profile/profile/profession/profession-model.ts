export interface ProfessionData{
    qualification: string,
    course: string,
    school: string,
    courseType: string,
    end: string,
    specialization: string,
    workerId: string
}