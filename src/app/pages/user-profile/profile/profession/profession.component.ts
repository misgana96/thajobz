import { Component, OnInit, Inject, ElementRef, ViewChild } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http'
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import {ProfessionService} from './profession.service'
import {AuthService} from '../../../../auth/auth.service'

@Component({
  selector: 'app-profession',
  templateUrl: './profession.component.html',
  styleUrls: ['./profession.component.scss']
})
export class ProfessionComponent implements OnInit {

  isSubmited: boolean;
  fileUrl: string | ArrayBuffer
  selectedFile: File;
  userId: string
  profData: any = {}
  public fileName: any ={}
  httpOptions: any


  @ViewChild("fileInput", {static: false}) fileInput: ElementRef;

  constructor(
            private router: Router, private professionservice: ProfessionService,
            private authservice: AuthService, private http: HttpClient){}

  ngOnInit(): void {
   this.professionservice.getUserProfession().subscribe(res=> {
     this.profData = res
   })
  this.professionservice.getProfessionId()
}

  uploadFile(event) {
    this.userId = this.authservice.currentUserValue['userId']
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.onload = (event) => this.fileUrl = event.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedFile = event.target.files[0];

    }
    else {
      this.fileUrl = null;
    }
    const file: HTMLInputElement = this.fileInput.nativeElement
    this.professionservice.uploadResume(file.files).subscribe(res=> {
      this.fileName = res['result'].files[this.selectedFile['name']][0].name
      this.fileUrl = `https://jobz6.herokuapp.com/api/files/docs/download/${this.fileName}`
      this.http.patch(`workers/${this.userId}`, {resumeUrl: this.fileUrl})
    })
  }

  onApply(form: NgForm) {
    this.isSubmited = true;
    const workerId = this.authservice.currentUserValue['userId']
    this.professionservice.insertProfession(form)
    .subscribe(
      res =>{
        this.router.navigate(['/profile/job-details'], {queryParams:{id:res}})
      })
  }
}
