import { Injectable, Inject } from '@angular/core';
import {ProfessionData} from './profession-model'
import {HttpClient, HttpHeaders} from '@angular/common/http'
import {AuthService} from '../../../../auth/auth.service'
import {UserInfoService} from '../my-info/user-info.service'
import {User} from '../../../../auth/user'
import { first } from 'rxjs/operators';



@Injectable({
    providedIn: 'root'
})

export class ProfessionService {
    httpOptions: any
    userId = this.authservice.currentUserValue['userId']
    id: any
    constructor (private authservice: AuthService, private http: HttpClient,
                 private userinfo: UserInfoService){}

    getUserProfession() {
        this.userId = this.authservice.currentUserValue['userId']
        const data={
           where:{
            workerId:this.userId
           }
        }
        const filter=JSON.stringify(data)
        return this.http.get<ProfessionData[]>(`education?filter=${filter}`);
    }


    uploadResume(file: FileList) {
        const formData = new FormData()
        for (let i = 0; i < file.length; i++) {
            formData.append(file[i].name, file[i])
        }
        return this.http.post<any>(`files/docs/upload`, formData)
    }
    getProfessionId() {
        this.userId = this.authservice.currentUserValue['userId']
        const data={
           where:{
            workerId:this.userId
           }
        }
        const filter=JSON.stringify(data)
        return this.http.get<any>(`education?filter=${filter}`)
        .subscribe(res=> {
            this.id = res[0].id
        })
    }

    insertProfession(value) {
        const user = this.authservice.currentUserValue
        this.userId = this.authservice.currentUserValue['userId']

        const data = {
            qualification: value.qualification,
            course: value.course,
            school: value.school,
            courseType: value.courseType,
            end: value.end,
            specialization: value.specialization,
            workerId: this.userId
        }
        if (user) {
            const cond={
               where:{
                workerId:this.userId
               }
            }
            const filter=JSON.stringify(cond)
            return this.http.patch<any>(`education/${this.id}`, data);
        }
    }

}