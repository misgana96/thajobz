import { Injectable, Inject} from '@angular/core'
import {HttpClient, HttpHeaders} from '@angular/common/http'
import {Setting} from './setting.model'
import {AuthService} from '../../../../auth/auth.service'



@Injectable({
    providedIn: 'root'
})

export class SettingService {
    constructor (private http: HttpClient, public authservice: AuthService){}


    changePassword(value) {
    	const data: Setting = {
    		oldPassword: value.oldPassword,
    		newPassword: value.newPassword,
    		confirmPassword: value.confirmPassword
    	}
        return this.http.post<any>(`workers/change-password`, data)
    }
} 