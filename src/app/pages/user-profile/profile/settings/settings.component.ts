import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import {SettingService} from './setting.service'
import {AuthService} from '../../../../auth/auth.service'


@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  items: Array<any>
  NewPassword: string
  OldPassword: string
  changePassword: Function
  user: Object
  @ViewChild('settingForm', {static: true})form: NgForm;

  constructor(
    public authservice: AuthService,
    public settingservice: SettingService,
    private router: Router 
    ){ 
     
  }

  ngOnInit(): void {
  }

  onApply(form: NgForm) {
    this.NewPassword = this.form.controls['newPassword'].value
    this.OldPassword = this.form.controls['oldPassword'].value
    this.settingservice.changePassword(form).subscribe(res=>{
      alert("password has changed Successfully")
    })
  }
}
