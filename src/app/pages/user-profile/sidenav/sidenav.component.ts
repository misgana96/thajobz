import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http'
import { ProfileService } from '../profile.service';
import {UserInfoService} from '../profile/my-info/user-info.service'
import {AuthService} from '../../../auth/auth.service'

import { Observable } from 'rxjs';
import {map} from 'rxjs/operators'


@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  userId: string
  imageUrl: string | ArrayBuffer
  username: string
  public sideNavItems = [
    { label: "My Info", path: '/profile/my-info' },
    { label: "Profession", path: '/profile/profession' },
    { label: "Job Detail", path: '/profile/job-details' },
    { label: "Notification", path: '/profile/notification' },
    { label: "Settings", path: '/profile/settings' },
  ]

  public image: Observable<any>;

  @Output() navSelected: EventEmitter<any> = new EventEmitter();
  
  constructor(private profile:ProfileService, private http: HttpClient, private authservice: AuthService,
              private userinfo: UserInfoService) { }

  ngOnInit(): void {

    this.userinfo.getImage.forEach(m=>{
        this.image = m
        console.log(this.image)
    })
  }

  navigation(navItem) {
    this.navSelected.emit(navItem)
  }

}
