// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'https://jobz6.herokuapp.com/api/',
  firebase: {
    apiKey: "AIzaSyBE7l0skQ50zeJVLl7hBQLOq_wQ0qjs1aI",
    authDomain: "thajz-back.firebaseapp.com",
    // databaseURL: "https://thajz-back.firebaseio.com",
    projectId: "thajz-back",
    // storageBucket: "thajz-back.appspot.com",
    messagingSenderId: "385547370238",
    appId: "1:385547370238:web:c4b1b28555d641f770efaf",
    measurementId: "G-P35VB7Q5M7",
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
